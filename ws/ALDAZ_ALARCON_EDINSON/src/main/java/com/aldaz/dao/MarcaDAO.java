package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.MarcaBean;

public interface MarcaDAO {
	public List<MarcaBean> listar();
}
