package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aldaz.entity.MarcaBean;

@Repository
public class MarcaDAOImpl implements MarcaDAO {

	@Autowired
	private SessionFactory factory;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<MarcaBean> listar() {
		Session session = factory.getCurrentSession();
		Query query = null;
		
		try {
			String hql = "from MarcaBean";
			query = session.createQuery(hql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return query.getResultList();
	}
}
