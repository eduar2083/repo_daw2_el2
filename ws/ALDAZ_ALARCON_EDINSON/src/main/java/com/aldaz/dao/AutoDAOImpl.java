package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aldaz.entity.AutoBean;

@Repository
public class AutoDAOImpl implements AutoDAO {
	
	@Autowired
	private SessionFactory factory;

	@Override
	@Transactional
	public AutoBean guardar(AutoBean bean) {
		Session sesion = factory.getCurrentSession();
		
		try {
			sesion.saveOrUpdate(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bean;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<AutoBean> filtrarXStock(int minimo, int maximo) {
		Session sesion = factory.getCurrentSession();
		Query query = null;
		
		try {
			String hql = "select auto from AutoBean auto inner join MarcaBean marca on auto.marca = marca.codMarca Where auto.stock between ?1 and ?2";
			query = sesion.createQuery(hql);
			query.setParameter(1, minimo == -1 ? 0 : minimo);
			query.setParameter(2, maximo == -1 ? Integer.MAX_VALUE : maximo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return query.getResultList();
	}

	@Override
	@Transactional
	public void eliminar(int id) {
		Session sesion = factory.getCurrentSession();
		
		try {
			AutoBean r = sesion.get(AutoBean.class, id);
			sesion.delete(r);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
