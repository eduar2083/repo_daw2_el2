package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TB_MARCA")
public class MarcaBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MarcaBean() {
		
	}

	public MarcaBean(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_MARCA")
	private int codMarca;
	
	@Column(name = "NOM_MARCA")
	private String nombre;
	
	@OneToMany(mappedBy = "marca")
	@JsonIgnore
	private List<AutoBean> autos;

	public int getCodMarca() {
		return codMarca;
	}

	public void setCodMarca(int codMarca) {
		this.codMarca = codMarca;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public List<AutoBean> getAutos() {
		return autos;
	}
	public void setAutos(List<AutoBean> autos) {
		this.autos = autos;
	}
}
