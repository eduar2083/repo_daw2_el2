package com.aldaz.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "TB_AUTO")
public class AutoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AutoBean() {
		
	}

	public AutoBean(String descripcion, double precio, int stock, MarcaBean marca) {
		super();
		this.descripcion = descripcion;
		this.precio = precio;
		this.stock = stock;
		this.marca = marca;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_AUTO")
	private int codAuto;
	
	@Column(name = "DES_AUTO")
	private String descripcion;
	
	@Column(name = "PRE_AUTO")
	private double precio;
	
	@Column(name = "STOCK_AUTO")
	private int stock;
	
	@JoinColumn(name = "COD_MARCA")
	@ManyToOne
	private MarcaBean marca;

	public int getCodAuto() {
		return codAuto;
	}

	public void setCodAuto(int codAuto) {
		this.codAuto = codAuto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public MarcaBean getMarca() {
		return marca;
	}

	public void setMarca(MarcaBean marca) {
		this.marca = marca;
	}
}
