package com.aldaz.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.AutoDAO;
import com.aldaz.entity.AutoBean;

@Service
public class AutoServiceImpl implements AutoService {
	
	@Autowired
	private AutoDAO dao;

	@Override
	public AutoBean guardar(AutoBean bean) {
		return dao.guardar(bean);
	}

	@Override
	public List<AutoBean> filtrarXStock(int minimo, int maximo) {
		return dao.filtrarXStock(minimo, maximo);
	}

	@Override
	public void eliminar(int id) {
		dao.eliminar(id);
	}
}
