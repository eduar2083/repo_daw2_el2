package com.aldaz.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.MarcaDAO;
import com.aldaz.entity.MarcaBean;

@Service
public class MarcaServiceImpl implements MarcaService {

	@Autowired
	private MarcaDAO dao;
	
	@Override
	public List<MarcaBean> listar() {
		return dao.listar();
	}
}
