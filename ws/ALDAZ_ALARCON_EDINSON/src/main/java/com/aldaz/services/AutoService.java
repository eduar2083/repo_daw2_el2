package com.aldaz.services;

import java.util.List;

import com.aldaz.entity.AutoBean;

public interface AutoService {
	
	public AutoBean guardar(AutoBean bean);
	
	public List<AutoBean> filtrarXStock(int minimo, int maximo);
	
	public void eliminar(int id);
}
