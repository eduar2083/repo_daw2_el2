package com.aldaz.services;

import java.util.List;

import com.aldaz.entity.MarcaBean;

public interface MarcaService {
	public List<MarcaBean> listar();
}
