package com.aldaz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aldaz.entity.AutoBean;
import com.aldaz.services.AutoService;

@Controller
@RequestMapping(value = "/autos")
public class AutoController {
	
	@Autowired
	private AutoService service;
	
	@RequestMapping("/")
	public String index() {
		return "auto";
	}

	@RequestMapping(value = "grabar", method = RequestMethod.POST)
	public @ResponseBody AutoBean guardar(AutoBean bean) {
		return service.guardar(bean);
	}

	@RequestMapping(value = "rangoStock")
	@ResponseBody
	public List<AutoBean> filtrarXStock(@RequestParam("min") int minimo, @RequestParam("max") int maximo) {
		List<AutoBean> lista = service.filtrarXStock(minimo, maximo);
		return lista;
	}

	@RequestMapping(value = "eliminar", method = RequestMethod.DELETE)
	@ResponseBody
	public int eliminar(@RequestParam("id") int id) {
		service.eliminar(id);
		
		return 1;
	}
}
