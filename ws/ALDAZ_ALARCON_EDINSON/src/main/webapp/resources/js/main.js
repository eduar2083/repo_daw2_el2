$(function() {
    fInicializar();
});

function fInicializar() {
    fConfigurarFormulario();
    fCargarMarcas();
    fConfiguraEventos();
    
    fListarDatos();
}

function fConfigurarFormulario() {
    window.validator = $("#frmRegistro").validate({
        rules: {
          descripcion: {
              required: true,
              minlength: 3,
              maxlength: 40
          },
          marca: {
              required: true
          },
          stock: {
        	  required: true,
              maxlength: 3,
              digits: true
          },
          precio: {
        	  required: true,
        	  maxlength: 10,
        	  number: true
          }
        },
        messages: {
        	descripcion: {
			   required: "Debe ingresar descripci&oacute;n",
			   minlength: "M&iacute;nimo {0} caracteres",
			   maxlength: "M&aacute;ximo {0} caracteres"
        	},
        	marca: {
			   required: "Debe seleccionar marca"
        	},
        	stock: {
			   required: "Debe ingresar stock",
			   maxlenth: "M&aacute;ximo {0} dígitos",
			   digits: "Debe ingresar un entero v&aacute;lido"
        	},
        	precio: {
			   required: "Debe ingresar precio",
			   number: "Debe ingresar un precio v&aacute;lido"
        	}
        }
  });
}

function fConfiguraEventos() {
    $('#frmRegistro').bind('submit', frmRegistro_submit);
    $('#frmBusqueda').bind('submit', function(e) {
    	e.preventDefault();
    	fListarDatos();
    });
    $('#frmEliminar').bind('submit', frmEliminar_submit);
    
    $('#btnLimpiar').bind('click', function(){
    	fLimpiarFormularioBusqueda();
    	fListarDatos();
    });
    
    $('#modalRegistro').on('show.bs.modal', function (e) {
    	fLimpiarFormularioRegistro();
    });
}

function fCargarMarcas() {
	axios.get('../marcas')
	.then(function(response) {
		response.data.map(function(e, i){
			$('#marca').append($('<option>', {
				value: e.codMarca,
				text: e.nombre
			}));
		});
	})
	.catch(function(response) {
		swal("Ha ocurrido un error!", "Consulte con el departamente de sistemas", "error");
	});
}

function frmRegistro_submit(e) {
    var isValid = $('#frmRegistro').valid();

    if (isValid) {
        e.preventDefault();
        
        $('#modalRegistro').modal('hide');
        
        let obj = {
            "descripcion": $('#descripcion').val(),
            "marca.codMarca": $('#marca').val(),
            "stock": $('#stock').val(),
            "precio": $('#precio').val()
        };
                
        $.post('grabar', obj, function(data) {
        	console.log(data);
        })
        .done(function(data) {
        	swal("Correcto!", "Se ha registrado!", "success");
        	fLimpiarFormularioRegistro();
        	fListarDatos();
        })
        .fail(function(data) {
        	swal("Ha ocurrido un error!", "Consulte con el departamento de sistemas", "error");
        });
    }
}

function fEliminar(codAuto, descripcion) {
	$('#lblDescripcion').text("<<" + descripcion + ">>");
	$('#hdCodAuto').val(codAuto);
	$('#modalEliminar').modal('show');
}

function frmEliminar_submit(e) {
	e.preventDefault();
    
    let obj = {
        "id": $('#hdCodAuto').val()
    };
            
    $.ajax({
    	url: 'eliminar?id=' + obj.id,
    	data: obj,
    	type: 'delete'
    })
    .done(function(data) {
    	$('#modalEliminar').modal('hide');
    	swal("Correcto!", "Se ha eliminado!", "success");
    	fListarDatos();
    })
    .fail(function(data) {
    	swal("Ha ocurrido un error!", "Consulte con el departamento de sistemas", "error");
    });
}

function fLimpiarFormularioRegistro() {
	$('#frmRegistro').trigger("reset");
	 window.validator.resetForm();
}

function fLimpiarFormularioBusqueda() {
	$('#stockMinimo,#stockMaximo').val('');
}

function fListarDatos() {
	let stockMinimo = $('#stockMinimo').val();
	let stockMaximo = $('#stockMaximo').val();
	
	let params = {
		min: stockMinimo == '' ? -1 : parseInt(stockMinimo),
		max: stockMaximo == '' ? -1 : parseInt(stockMaximo)
	};
	
	$.get("rangoStock", params)
	.done(function(data) {
		$('#tbListado tbody').html('');
		data.map(function(e, i) {
			$('#tbListado tbody').append(`<tr><td>${e.codAuto}</td><td>${e.descripcion}</td><td>${e.marca.nombre}</td><td>${e.stock}</td><td>${e.precio}</td><td><button class='btn btn-sm btn-danger' type='button' onclick='fEliminar(${e.codAuto}, "${e.descripcion}")'>Eliminar</button></td></tr>`);
		});
    })
    .fail(function(data) {
    	swal("Ha ocurrido un error!", "Consulte con el departamento de sistemas", "error");
    });
}