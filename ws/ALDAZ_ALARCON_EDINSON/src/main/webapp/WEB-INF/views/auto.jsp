<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charse=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CL 3 - DSW II</title>
<link rel="shortcut icon" type="image/x-icon"
	href="../resources/img/favicon.ico" />
<link rel="stylesheet"
	href="../resources/libs/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet"
	href="../resources/libs/font-awesome-5-css/css/all.min.css">
<style>
.error {
	color: red;
}
</style>
</head>

<body>
	<div class="container">
		<div class="card">
			<div class="card-header bg-dark text-white text-center">
				<h4>Mantenedor de Autos</h4>
			</div>
			<div class="card-body">
				<nav class="navbar navbar-light bg-light justify-content-between">
					<button type="button" class="btn btn-warning" data-toggle="modal"
						data-target="#modalRegistro" data-whatever="@mdo">
						<i class="fas fa-plus mr-1"></i> Nuevo
					</button>
					<form class="form-inline" id="frmBusqueda" autocomplete="off">
						<input class="form-control mr-sm-2" type="search"
							placeholder="stock m�nimo" aria-label="edad m�nima"
							id="stockMinimo"> <input class="form-control mr-sm-2"
							type="search" placeholder="stock m�ximo" aria-label="edad m�xima"
							id="stockMaximo">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
						<button class="btn btn-outline-secondary my-2 my-sm-0"
							type="button" id="btnLimpiar">Limpiar</button>
					</form>
				</nav>
				<table class="table table-stripped table-hover table-info mt-2"
					id="tbListado">
					<thead>
						<tr>
							<td>Id</td>
							<td>Descripci�n</td>
							<td>Marca</td>
							<td>Stock</td>
							<td>Precio</td>
							<td>Acci�n</td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog"
		aria-labelledby="modalRegistroLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form id="frmRegistro" accept-charset="UTF-8" method="post"
					autocomplete="off">
					<div class="modal-header bg-dark text-white p-1">
						<h5 class="modal-title" id="exampleModalLabel">Registrar
							nuevo auto</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="descripcion" class="col-form-label">Descripci�n:</label>
								<input type="text" class="form-control" id="descripcion"
									name="descripcion"> <span class="error"
									for="descripcion"></span>
							</div>
							<div class="form-group col-md-6">
								<label for="marca" class="col-form-label">Marca:</label> <select
									class="custom-select" id="marca" name="marca">
									<option value="">::: Seleccione :::</option>
								</select> <span class="error" for="marca"></span>
							</div>
							<div class="form-group col-md-6">
								<label for="stock" class="col-form-label">Stock:</label> <input
									type="number" class="form-control" id="stock" name="stock">
								<span class="error" for="stock"></span>
							</div>
							<div class="form-group col-md-6">
								<label for="precio" class="col-form-label">Precio:</label> <input
									type="text" class="form-control" id="precio" name="precio">
								<span class="error" for="precio"></span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-danger">Crear</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog"
		aria-labelledby="modalEliminarLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger text-white p-1">
					<h5 class="modal-title" id="exampleModalLabel">Eliminar auto</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Est� seguro de eliminar el auto <label id="lblDescripcion"></label></p>
				</div>
				<div class="modal-footer">
					<form id="frmEliminar">
						<input type="hidden" id="hdCodAuto" />
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">No</button>
						<button type="submit" class="btn btn-danger">Si</button>
					</form>

				</div>
			</div>
		</div>
	</div>

	<script src="../resources/libs/jquery/dist/jquery.min.js"></script>
	<script src="../resources/libs/popperjs/umd/popper.min.js"></script>
	<script
		src="../resources/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script
		src="../resources/libs/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="../resources/libs/axios/axios.min.js"></script>
	<script src="../resources/js/main.js"></script>
</body>

</html>